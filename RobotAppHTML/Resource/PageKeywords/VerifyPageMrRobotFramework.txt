*** Settings ***
Library           Selenium2Library
Library           Collections

*** Keywords ***
Verify Event Page
    Page Should Contain    MR. Robot Framework
    Page Should Contain    Member
    Page Should Contain    Event : Signature Cha-am
    Element Should Be Visible    xpath=//img[@src="https://t-ec.bstatic.com/images/hotel/max1024x768/963/96328218.jpg"]
    Element Should Be Visible    xpath=//img[@src="http://www.baanpuckhuahin.com/wp-content/uploads/2016/07/baan_Signature_08.jpg"]
    Element Should Be Visible    xpath=//img[@src="http://www.xn--12c1cbbv9c1ab4ccf6a6v.net/wp-content/uploads/2016/07/baan_Signature_07.jpg"]
    Page Should Contain    Automate on the beach
    sleep    2s
    Capture Page Screenshot

Verify Members Ja
    sleep    5s
    Page Should Contain    Member
    @{Members}    Create List
    ${count}    Get Matching Xpath Count    //a[@href="#"]
    : FOR    ${i}    IN RANGE    1    ${count}+1
    \    ${MemberName}=    Get Text    xpath=(//a[@href="#"])[${i}]
    \    Append To List    ${Members}    ${MemberName}
    \    Comment    Log List    ${Members}
