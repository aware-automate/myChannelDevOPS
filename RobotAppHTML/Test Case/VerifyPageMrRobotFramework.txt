*** Settings ***
Test Teardown     Close Browser
Library           Selenium2Library
Library           Collections
Resource          ../Resource/PageKeywords/VerifyPageMrRobotFramework.txt
Resource          ../Resource/PageVariables/VerifyPageMrRobotFrameworkPageVariables.txt

*** Test Cases ***
Verify Header
    Open Browser    ${LinkWebHtml}    gc
    Maximize Browser Window
    sleep    5s
    Capture Page Screenshot
    Verify Event Page

Verify Members
    [Tags]    workaround
    Open Browser    ${LinkWebHtml}    gc
    Maximize Browser Window
    Verify Members Ja
